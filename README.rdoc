== Welcome to Worship Tuner


TODO Items:
 - paginate results for show page of a given tag
 - add a tag cloud to the blog index page (only)
 - add tests for controllers
 - add tests for helpers


Research:
 - Research possibly changing the chart mapping to drag and drop items instead of a text field.
 - Research the ability to dynamically change the chords on a chart.